# RESTful spring-boot application with jquery datatable (datatables.net) adapted for wildfly 11

Build war for wildfly 11:
```
/mvnw -P prod clean install -DskipTest
```
Or start with embedded jetty in debug mode:
```
./mvnw spring-boot:run -Dspring-boot.run.profiles=dev -DskipTests
```
After deploy to wildfly 11 open in your browser:
```
http://localhost:8080/example/
```
 Swagger:
 ```
 http://localhost:8080/example/swagger-ui.html
 ```
 Actuator:
 ```
 http://localhost:8080/example/actuator
 ```

