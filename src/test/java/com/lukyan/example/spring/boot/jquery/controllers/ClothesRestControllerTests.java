package com.lukyan.example.spring.boot.jquery.controllers;

import com.lukyan.example.spring.boot.jquery.Application;
import com.lukyan.example.spring.boot.jquery.dtos.rest.ClothesDto;
import com.lukyan.example.spring.boot.jquery.enums.ColorEnum;
import com.lukyan.example.spring.boot.jquery.enums.SizeEnum;
import com.lukyan.example.spring.boot.jquery.enums.StateEnum;
import com.lukyan.example.spring.boot.jquery.enums.TypeEnum;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        properties = {"server.port=18087"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClothesRestControllerTests {

    @Autowired
    @Qualifier("testRestTemplate")
    private TestRestTemplate testRestTemplate;

    private static ClothesDto dto;

    @Before
    public void setUp() {
        dto = new ClothesDto();
        dto.setSize(SizeEnum.SIZE42.value());
        dto.setPrice(45.65);
        dto.setColor(ColorEnum.BLACK.value());
        dto.setType(TypeEnum.PANTS.value());
        dto.setDescription("Black pants description");
        dto.setState(StateEnum.STORAGE.value());
    }

    @Test
    public void test001ClothesRestControllerAddPositive001() {
    }

}

