package com.lukyan.example.spring.boot.jquery.services;

import com.lukyan.example.spring.boot.jquery.Application;
import com.lukyan.example.spring.boot.jquery.dtos.rest.ClothesDto;
import com.lukyan.example.spring.boot.jquery.enums.ColorEnum;
import com.lukyan.example.spring.boot.jquery.enums.SizeEnum;
import com.lukyan.example.spring.boot.jquery.enums.StateEnum;
import com.lukyan.example.spring.boot.jquery.enums.TypeEnum;
import com.lukyan.example.spring.boot.jquery.exceptions.InvalidParamException;
import com.lukyan.example.spring.boot.jquery.jpa.entity.Clothes;
import com.lukyan.example.spring.boot.jquery.services.api.ClothesService;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        properties = {"server.port=18087"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClothesServiceTest {

    @Autowired
    private ClothesService clothesService;

    private static ClothesDto dto;

    @Before
    public void setUp() {
        dto = new ClothesDto();
        dto.setSize(SizeEnum.SIZE42.value());
        dto.setPrice(45.65);
        dto.setColor(ColorEnum.BLACK.value());
        dto.setType(TypeEnum.PANTS.value());
        dto.setDescription("Black pants description");
        dto.setState(StateEnum.STORAGE.value());
    }

    @Test
    @Ignore
    public void test001ClothesServiceVerifyColorPositive001() {
        clothesService.verifyColor(ColorEnum.BLACK.value());
    }

    @Test(expected = InvalidParamException.class)
    @Ignore
    public void test002ClothesServiceVerifyColorNegative001() {
        clothesService.verifyColor("incorrect");
    }

    @Test(expected = InvalidParamException.class)
    @Ignore
    public void test003ClothesServiceVerifyColorNegative002() {
        clothesService.verifyColor(null);
    }

    @Test
    @Ignore
    public void test004ClothesServiceVerifySizePositive001() {
        clothesService.verifySize(45);
    }

    @Test(expected = InvalidParamException.class)
    @Ignore
    public void test005ClothesServiceVerifySizeNegative001() {
        clothesService.verifySize(40);
    }

    @Test(expected = InvalidParamException.class)
    @Ignore
    public void test006ClothesServiceVerifySizeNegative002() {
        clothesService.verifySize(null);
    }

/*
    @Test
    public void test007ClothesServiceAddPositive001() {
        Clothes clothes = clothesService.save(dto);
        assertNotNull(clothes);
        assertNotNull(clothes.getId());
    }
*/

    @Test
    @Ignore
    public void test008ClothesServiceGetOnePositive001() {
        Long id = 1L;
        Clothes clothes = clothesService.getOne(id);
        assertNotNull(clothes);
        assertTrue(clothes.getId() == id);
    }

    @Test
    @Ignore
    public void test009ClothesServiceFindAllPositive001() {
        List<Clothes> clothesList = clothesService.findAll();
        assertNotNull(clothesList);
        assertTrue(clothesList.size() == 6);
    }

    @Test
    @Ignore
    public void test010ClothesServiceFindAllPositive002() {
        List<Clothes> clothesList = clothesService.findAll(null, null, null, null, null);
        assertNotNull(clothesList);
        assertTrue(clothesList.size() == 6);
    }

    @Test
    @Ignore
    public void test011ClothesServiceDeletePositive001() {
        clothesService.delete(1L);
        List<Clothes> clothesList = clothesService.findAll();
        assertNotNull(clothesList);
        assertTrue(clothesList.size() == 5);
    }

/*
    @Test
    public void test012ClothesServiceEditPositive001() {
        dto.setId(51L);
        dto.setPrice(1001.00);
        dto.setColor(ColorEnum.WHITE.value());
        Map<Integer, ClothesDto> map = new HashMap<>();
        map.put(1, dto);
        JsEditorDto jsEditorDto = new JsEditorDto();
        jsEditorDto.setAction(JsEditorActionEnum.EDIT.value());
        jsEditorDto.setData(map);
        Clothes clothes = clothesService.save(jsEditorDto);
        assertNotNull(clothes);
        assertTrue(clothes.getPrice() > 1000d);
    }
*/

/*
     @Test
     @Ignore
    public void test013ClothesServiceMovePositive001() {
        Clothes clothes = clothesService.move(51L);
        assertNotNull(clothes);
        assertTrue(clothes.getState().value().equals(StateEnum.SHOP.value()));
    }
*/

}

