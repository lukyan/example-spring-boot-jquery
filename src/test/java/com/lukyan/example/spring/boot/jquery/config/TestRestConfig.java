package com.lukyan.example.spring.boot.jquery.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class TestRestConfig {

    @Bean(name = "testRestTemplate")
    public TestRestTemplate testRestTemplate(@Qualifier("restTemplate") RestTemplate restTemplate) {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        restTemplateBuilder.configure(restTemplate);
        TestRestTemplate testRestTemplate = new TestRestTemplate(restTemplateBuilder);
        return testRestTemplate;
    }
}
