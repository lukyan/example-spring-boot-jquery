package com.lukyan.example.spring.boot.jquery.exceptions;

public class RequestBodyException extends BadRequestException {
    public RequestBodyException(String errorMessage) {
        super(INCORRECT_REQUEST_BODY, errorMessage);
    }

    public RequestBodyException(String errorMessage, Throwable throwable) {
        super(INCORRECT_REQUEST_BODY, errorMessage, throwable);
    }
}
