package com.lukyan.example.spring.boot.jquery.enums;

public enum StateEnum {
    STORAGE("storage"),
    SHOP("shop");

    private String value;

    StateEnum(String value) {
        this.value = value;
    }

    public static StateEnum byValue(String value) {
        for (StateEnum item : StateEnum.values()) {
            if (item.value.equals(value)) {
                return item;
            }
        }
        return null;
    }

    public String value() {
        return value;
    }
}
