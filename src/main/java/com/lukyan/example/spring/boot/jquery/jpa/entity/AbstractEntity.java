package com.lukyan.example.spring.boot.jquery.jpa.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lukyan.example.spring.boot.jquery.jpa.entity.converter.JPAInstantConverter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {
    @Getter
    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Column(name = "CREATED_AT", nullable = false, columnDefinition = "TIMESTAMP")
    @Convert(converter = JPAInstantConverter.class)
    private Instant createdAt;

    @Getter
    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Column(name = "UPDATED_AT", nullable = false, columnDefinition = "TIMESTAMP")
    @Convert(converter = JPAInstantConverter.class)
    private Instant updatedAt;
}
