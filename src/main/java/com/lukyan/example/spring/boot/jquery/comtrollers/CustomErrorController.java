package com.lukyan.example.spring.boot.jquery.comtrollers;

import com.lukyan.example.spring.boot.jquery.dtos.rest.ErrorResponse;
import com.lukyan.example.spring.boot.jquery.helpers.RestHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomErrorController implements ErrorController {

    private final static Logger log = LoggerFactory.getLogger(CustomErrorController.class);

    private static final String PATH = "/error";

    @RequestMapping(value = PATH, method = RequestMethod.GET)
    public ResponseEntity<ErrorResponse> error() {
        ErrorResponse errorResponse = RestHelper.instance().
                errorResponse(new RuntimeException(HttpStatus.NOT_FOUND.getReasonPhrase()));
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
