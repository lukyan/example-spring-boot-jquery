package com.lukyan.example.spring.boot.jquery.helpers;

import com.lukyan.example.spring.boot.jquery.dtos.rest.ErrorResponse;

import java.io.PrintWriter;
import java.io.StringWriter;

import static com.lukyan.example.spring.boot.jquery.dtos.rest.ErrorResponse.INTERNAL_SERVER_ERROR;

public class RestHelper {

    private RestHelper() {
    }

    private static class SingletonHolder {
        static final RestHelper INSTANCE = new RestHelper();
    }

    public static RestHelper instance() {
        return SingletonHolder.INSTANCE;
    }

    public ErrorResponse errorResponse(Throwable throwable) {
        String stackTrace = stackTraceToString(throwable);
        return new ErrorResponse(INTERNAL_SERVER_ERROR,
                throwable.getMessage(), stackTrace);
    }

    public String stackTraceToString(Throwable e) {
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }
}
