package com.lukyan.example.spring.boot.jquery.convertors;

import com.lukyan.example.spring.boot.jquery.dtos.rest.ClothesDto;
import com.lukyan.example.spring.boot.jquery.jpa.entity.Clothes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EntitiesToClothesDtosConverter implements Converter<List<Clothes>, List<ClothesDto>> {

    @Autowired
    private EntityToClothesDtoConverter entityToClothesDtoConverter;

    @Override
    public List<ClothesDto> convert(List<Clothes> entities) {
        List<ClothesDto> dtos = new ArrayList<>();
        for (Clothes entity : entities) {
            ClothesDto dto = entityToClothesDtoConverter.convert(entity);
            dtos.add(dto);
        }
        return dtos;
    }
}
