package com.lukyan.example.spring.boot.jquery.comtrollers;

import com.lukyan.example.spring.boot.jquery.convertors.EntitiesToClothesDtosConverter;
import com.lukyan.example.spring.boot.jquery.convertors.EntityToClothesDtoConverter;
import com.lukyan.example.spring.boot.jquery.dtos.rest.ClothesDto;
import com.lukyan.example.spring.boot.jquery.dtos.rest.SelectDto;
import com.lukyan.example.spring.boot.jquery.enums.ColorEnum;
import com.lukyan.example.spring.boot.jquery.enums.SizeEnum;
import com.lukyan.example.spring.boot.jquery.enums.StateEnum;
import com.lukyan.example.spring.boot.jquery.enums.TypeEnum;
import com.lukyan.example.spring.boot.jquery.exceptions.ValueIsNullException;
import com.lukyan.example.spring.boot.jquery.jpa.entity.Clothes;
import com.lukyan.example.spring.boot.jquery.services.api.ClothesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class ClothesRestController {

    private static Logger log = LoggerFactory.getLogger(ClothesRestController.class);

    @Autowired
    private ClothesService clothesService;

    @Autowired
    private EntityToClothesDtoConverter entityToClothesDtoConverter;

    @Autowired
    private EntitiesToClothesDtosConverter entitiesToClothesDtosConverter;

    @GetMapping("/{id}")
    public ResponseEntity<ClothesDto> get(HttpServletRequest req,
                                          @PathVariable Long id) {
        Clothes clothes = clothesService.getOne(id);
        if (clothes == null) {
            throw new ValueIsNullException(String.format("Clothes with id %s not found", id));
        }
        ClothesDto clothesDto = entityToClothesDtoConverter.convert(clothes);
        return new ResponseEntity<>(clothesDto, HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<List<ClothesDto>> get(HttpServletRequest req,
                                                @RequestParam(name = "sortBy", required = false) String sortBy,
                                                @RequestParam(name = "sortOrder", required = false) String sortOrder,
                                                @RequestParam(name = "offset", required = false) Integer offset,
                                                @RequestParam(name = "limit", required = false) Integer limit,
                                                @RequestParam(name = "keyword", required = false) String keyword) {
        List<Clothes> clothesList = clothesService.findAll(sortBy, sortOrder, offset, limit, keyword);
        if (clothesList == null || clothesList.isEmpty()) {
            throw new ValueIsNullException("Clothes list is empty or null");
        }
        List<ClothesDto> dtos = entitiesToClothesDtosConverter.convert(clothesList);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(HttpServletRequest req,
                                    @PathVariable Long id) {
        clothesService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClothesDto> save(HttpServletRequest req,
                                           @RequestBody String requestBody) {
        Map<String, Map> map = clothesService.resolveMap(requestBody);
        Clothes clothes = clothesService.save(map);
        ClothesDto clothesDto = entityToClothesDtoConverter.convert(clothes);
        return new ResponseEntity<>(clothesDto, HttpStatus.OK);
    }

    @PostMapping(value = "/move", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClothesDto> move(HttpServletRequest req,
                                           @RequestBody String requestBody) {
        ClothesDto clothesDto = clothesService.resolveClothesDto(requestBody);
        Clothes clothes = clothesService.move(clothesDto);
        ClothesDto resultClothesDto = entityToClothesDtoConverter.convert(clothes);
        return new ResponseEntity<>(resultClothesDto, HttpStatus.OK);
    }

    @GetMapping(value = "/sizes", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<SelectDto>> sizes(HttpServletRequest req) {
        List<SelectDto> dtos = new ArrayList<>();
        for (SizeEnum sizeEnum : SizeEnum.values()) {
            SelectDto dto = new SelectDto();
            dto.setLabel(sizeEnum.name());
            dto.setValue(String.valueOf(sizeEnum.value()));
            dtos.add(dto);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/colors", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<SelectDto>> colors(HttpServletRequest req) {
        List<SelectDto> dtos = new ArrayList<>();
        for (ColorEnum colorEnum : ColorEnum.values()) {
            SelectDto dto = new SelectDto();
            dto.setLabel(colorEnum.name());
            dto.setValue(colorEnum.value());
            dtos.add(dto);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/types", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<SelectDto>> types(HttpServletRequest req) {
        List<SelectDto> dtos = new ArrayList<>();
        for (TypeEnum typeEnum : TypeEnum.values()) {
            SelectDto dto = new SelectDto();
            dto.setLabel(typeEnum.name());
            dto.setValue(typeEnum.value());
            dtos.add(dto);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/states", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<SelectDto>> states(HttpServletRequest req) {
        List<SelectDto> dtos = new ArrayList<>();
        for (StateEnum stateEnum : StateEnum.values()) {
            SelectDto dto = new SelectDto();
            dto.setLabel(stateEnum.name());
            dto.setValue(stateEnum.value());
            dtos.add(dto);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

}
