package com.lukyan.example.spring.boot.jquery.exceptions;

public class InvalidParamException extends BadRequestException {

    public InvalidParamException(String errorMessage) {
        super(INVALID_PARAM, errorMessage);
    }
}
