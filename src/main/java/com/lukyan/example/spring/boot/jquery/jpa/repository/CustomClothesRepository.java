package com.lukyan.example.spring.boot.jquery.jpa.repository;

import com.lukyan.example.spring.boot.jquery.jpa.entity.Clothes;

import java.util.List;

public interface CustomClothesRepository {
    List<Clothes> findAll(String sortBy, String sortOrder,
                          Integer offset, Integer limit,
                          String keyword);
}