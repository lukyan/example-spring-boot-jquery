package com.lukyan.example.spring.boot.jquery.exceptions;

public class ValueIsNullException extends BadRequestException {

    //TODO may be move hardcoded message to i18 resource
    private static String message = "Value is null";

    public ValueIsNullException() {
        super(VALUE_IS_NULL, message);
    }

    public ValueIsNullException(String message) {
        super(VALUE_IS_NULL, message);
    }
}
