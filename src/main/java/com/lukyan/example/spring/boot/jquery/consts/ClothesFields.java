package com.lukyan.example.spring.boot.jquery.consts;

public interface ClothesFields {
    String ID = "id";
    String SIZE = "size";
    String PRICE = "price";
    String COLOR = "color";
    String TYPE = "type";
    String DESCRIPTION = "description";
    String STATE = "state";
}
