package com.lukyan.example.spring.boot.jquery.dtos.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
    public static final String INTERNAL_SERVER_ERROR = "internal_server_error";
    public static final String REQUIRED = "required";
    public static final String INVALID = "invalid";
    public static final String INVALID_FORMAT = "invalid_format";
    public static final String INVALID_RANGE = "invalid_range";

    @Getter
    @Setter
    @NonNull
    private String errorKey;
    @Getter
    @Setter
    @NonNull
    private String errorMessage;
    @Getter
    @Setter
    private String developerMessage;
}
