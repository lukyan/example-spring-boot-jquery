package com.lukyan.example.spring.boot.jquery.comtrollers;

import com.lukyan.example.spring.boot.jquery.dtos.rest.ErrorResponse;
import com.lukyan.example.spring.boot.jquery.exceptions.BadRequestException;
import com.lukyan.example.spring.boot.jquery.helpers.RestHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalControllerExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

    @ExceptionHandler(value = {BadRequestException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse badRequestException(BadRequestException ex) {
        log.error(ex.getMessage(), ex);
        return RestHelper.instance().errorResponse(ex);
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse illegalArgumentException(IllegalArgumentException ex) {
        log.error(ex.getMessage(), ex);
        return RestHelper.instance().errorResponse(ex);
    }

    @ExceptionHandler(value = {NullPointerException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse nullPointerException(NullPointerException ex) {
        log.error(ex.getMessage(), ex);
        return RestHelper.instance().errorResponse(ex);
    }
}