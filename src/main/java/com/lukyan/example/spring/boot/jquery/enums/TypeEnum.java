package com.lukyan.example.spring.boot.jquery.enums;

public enum TypeEnum {
    DRESS("dress"),
    PANTS("pants"),
    SKIRT("skirt"),
    VEST("vest"),
    SHIRT("shirt");

    private String value;

    TypeEnum(String value) {
        this.value = value;
    }

    public static TypeEnum byValue(String value) {
        for (TypeEnum item : TypeEnum.values()) {
            if (item.value.equals(value)) {
                return item;
            }
        }
        return null;
    }

    public String value() {
        return value;
    }
}
