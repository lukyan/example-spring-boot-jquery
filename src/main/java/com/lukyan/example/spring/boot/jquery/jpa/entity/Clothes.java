package com.lukyan.example.spring.boot.jquery.jpa.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.lukyan.example.spring.boot.jquery.enums.ColorEnum;
import com.lukyan.example.spring.boot.jquery.enums.SizeEnum;
import com.lukyan.example.spring.boot.jquery.enums.StateEnum;
import com.lukyan.example.spring.boot.jquery.enums.TypeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "CLOTHES")
@JsonInclude(JsonInclude.Include.NON_ABSENT)
@NoArgsConstructor
public class Clothes extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    @Id
    @SequenceGenerator(name = "seq_clothes_id_generator",
            sequenceName = "SEQ_CLOTHES_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "seq_clothes_id_generator")
    @Column(name = "ID")
    private Long id;

    @Getter
    @Setter
    @Column(name = "SIZE", nullable = false)
    @Enumerated(EnumType.STRING)
    private SizeEnum size;

    @Getter
    @Setter
    @Column(name = "PRICE")
    private Double price;

    @Getter
    @Setter
    @Column(name = "COLOR", nullable = false)
    @Enumerated(EnumType.STRING)
    private ColorEnum color;

    @Getter
    @Setter
    @Column(name = "TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private TypeEnum type;

    @Getter
    @Setter
    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Getter
    @Setter
    @Column(name = "STATE", nullable = false)
    @Enumerated(EnumType.STRING)
    private StateEnum state;
}
