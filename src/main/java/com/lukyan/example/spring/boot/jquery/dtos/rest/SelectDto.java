package com.lukyan.example.spring.boot.jquery.dtos.rest;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

public class SelectDto {
    @NonNull
    @Getter
    @Setter
    private String label;
    @NonNull
    @Getter
    @Setter
    private String value;
}
