package com.lukyan.example.spring.boot.jquery.dtos.rest;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

public class ClothesDto {
    @Getter
    @Setter
    private Long id;
    @NonNull
    @Getter
    @Setter
    private Integer size;
    @Getter
    @Setter
    private Double price;
    @NonNull
    @Getter
    @Setter
    private String color;
    @NonNull
    @Getter
    @Setter
    private String type;
    @NonNull
    @Getter
    @Setter
    private String description;
    @NonNull
    @Getter
    @Setter
    private String state;
}
