package com.lukyan.example.spring.boot.jquery.exceptions;

public class BadRequestException extends AbstractException {

    private static final String BAD_REQUEST = "bad_request";
    protected static final String INCORRECT_REQUEST_BODY = "incorrect_request_body";
    protected static final String INVALID_PARAM = "invalid_param";
    protected static final String ALREADY_EXISTS = "already_exists";
    protected static final String REQUEST_PARAM_LOST = "request_param_lost";
    protected static final String WRONG_FORM_PARAMETER = "wrong_form_parameter";
    protected static final String UNABLE_TO_READ = "unable_to_read";
    protected static final String UNABLE_TO_CREATE = "unable_to_create";
    protected static final String UNABLE_TO_UPDATE = "unable_to_update";
    protected static final String UNABLE_TO_DELETE = "unable_to_delete";
    protected static final String VALUE_IS_NULL = "value_is_null";
    protected static final String PARAM_IS_INCORRECT = "param_is_incorrect";

    protected BadRequestException(String errorKey, String errorMessage) {
        super(errorKey, errorMessage);
    }

    protected BadRequestException(String errorKey, String errorMessage, Throwable throwable) {
        super(errorKey, errorMessage, throwable);
    }
}
