package com.lukyan.example.spring.boot.jquery.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lukyan.example.spring.boot.jquery.convertors.ClothesDtoToEntityConverter;
import com.lukyan.example.spring.boot.jquery.convertors.MapToClothesDtoConverter;
import com.lukyan.example.spring.boot.jquery.dtos.rest.ClothesDto;
import com.lukyan.example.spring.boot.jquery.enums.ColorEnum;
import com.lukyan.example.spring.boot.jquery.enums.SizeEnum;
import com.lukyan.example.spring.boot.jquery.enums.StateEnum;
import com.lukyan.example.spring.boot.jquery.enums.TypeEnum;
import com.lukyan.example.spring.boot.jquery.exceptions.InvalidParamException;
import com.lukyan.example.spring.boot.jquery.exceptions.RequestBodyException;
import com.lukyan.example.spring.boot.jquery.jpa.entity.Clothes;
import com.lukyan.example.spring.boot.jquery.jpa.repository.ClothesRepository;
import com.lukyan.example.spring.boot.jquery.jpa.repository.CustomClothesRepository;
import com.lukyan.example.spring.boot.jquery.services.api.ClothesService;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service("clothesService")
public class ClothesServiceImpl implements ClothesService {

    private final static Logger log = LoggerFactory.getLogger(ClothesServiceImpl.class);

    @Autowired
    @Qualifier("jacksonObjectMapper")
    private ObjectMapper jacksonObjectMapper;

    @Autowired
    private ClothesRepository clothesRepository;

    @Autowired
    private CustomClothesRepository customClothesRepository;

    @Autowired
    private MapToClothesDtoConverter mapToClothesDtoConverter;

    @Override
    public ClothesDto resolveClothesDto(String jsonRequestBody) {
        log.debug("Resolving ClothesDto...");
        ClothesDto dto;
        try {
            dto = jacksonObjectMapper.readValue(jsonRequestBody, ClothesDto.class);
        } catch (Exception e) {
            //TODO may be move hardcoded message to i18 resource
            throw new RequestBodyException("incorrect request body");
        }
        return dto;
    }

    @Override
    public void checkParam(Object param, String paramName) {
        log.debug(String.format("Check %s param...", paramName));
        if (param instanceof String) {
            if (param == null || String.valueOf(param).isEmpty()) {
                //TODO may be move hardcoded message to i18 resource
                throw new InvalidParamException(String.format("parameter '%s' have no value", paramName));
            }
        } else {
            if (param == null) {
                //TODO may be move hardcoded message to i18 resource
                throw new InvalidParamException(String.format("parameter '%s' have no value", paramName));
            }
        }
    }

    @Override
    public void verifySize(Integer size) {
        for (SizeEnum item : SizeEnum.values()) {
            if (size == null || size < 42 || size > 54) {
                //TODO may be move hardcoded message to i18 resource
                throw new InvalidParamException("parameter 'size' is incorrect (must be 42-54)");
            }
        }
    }

    @Override
    public void verifyColor(String color) {
        boolean isValid = false;
        for (ColorEnum item : ColorEnum.values()) {
            if (item.value().equals(color)) {
                isValid = true;
            }
        }
        if (!isValid) {
            //TODO may be move hardcoded message to i18 resource
            throw new InvalidParamException("parameter 'color' is incorrect");
        }
    }

    @Override
    public void verifyType(String type) {
        boolean isValid = false;
        for (TypeEnum item : TypeEnum.values()) {
            if (item.value().equals(type)) {
                isValid = true;
            }
        }
        if (!isValid) {
            //TODO may be move hardcoded message to i18 resource
            throw new InvalidParamException("parameter 'type' is incorrect");
        }
    }

    @Override
    public void verifyState(String state) {
        boolean isValid = false;
        for (StateEnum item : StateEnum.values()) {
            if (item.value().equals(state)) {
                isValid = true;
            }
        }
        if (!isValid) {
            //TODO may be move hardcoded message to i18 resource
            throw new InvalidParamException("parameter 'state' is incorrect");
        }
    }

    @Override
    public Clothes getOne(Long id) {
        return clothesRepository.getOne(id);
    }

    @Override
    public List<Clothes> findAll(String sortBy, String sortOrder, Integer offset, Integer limit, String keyword) {
        return customClothesRepository.findAll(sortBy, sortOrder, offset, limit, keyword);
    }

    @Override
    public List<Clothes> findAll() {
        return clothesRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        clothesRepository.deleteById(id);
    }

    public Clothes extractClothesFromMap(@NonNull Map<String, Map> mapParam) {
        //TODO check null
        Map<String, Map<String, String>> clothesMaps = mapParam.get("data");
        if (clothesMaps == null) {
            //TODO throw exception
        }
        Clothes entity = null;
        Set<String> keySet = clothesMaps.keySet();
        for (String key : keySet) {
            Map<String, String> clothesMap = clothesMaps.get(key);
            ClothesDto clothesDto = mapToClothesDtoConverter.convert(clothesMap);
            Instant now = Instant.now();
            if (clothesDto.getId() == null) {
                entity = new Clothes();
                entity.setCreatedAt(now);
            } else {
                entity = clothesRepository.getOne(clothesDto.getId());
            }
            entity.setSize(SizeEnum.byValue(clothesDto.getSize()));
            entity.setPrice(clothesDto.getPrice());
            entity.setColor(ColorEnum.byValue(clothesDto.getColor()));
            entity.setType(TypeEnum.byValue(clothesDto.getType()));
            entity.setDescription(clothesDto.getDescription());
            entity.setState(StateEnum.byValue(clothesDto.getState()));
            entity.setUpdatedAt(now);
        }
        return entity;
    }

    @Transactional(propagation = Propagation.NESTED, isolation = Isolation.READ_COMMITTED)
    @Override
    public Clothes save(@NonNull Map<String, Map> mapParam) {
        Clothes entity = extractClothesFromMap(mapParam);
        entity = clothesRepository.save(entity);
        return entity;
    }

    @Transactional(propagation = Propagation.NESTED, isolation = Isolation.READ_COMMITTED)
    @Override
    public Clothes move(ClothesDto clothesDto) {
        Clothes entity = clothesRepository.getOne(clothesDto.getId());
        if(entity != null) {
            if (StateEnum.STORAGE.value().equals(entity.getState().value())) {
                entity.setState(StateEnum.SHOP);
            } else {
                entity.setState(StateEnum.STORAGE);
            }
            entity = clothesRepository.save(entity);
        } else {
            //TODO throw exception
        }
        return entity;
    }

    @Override
    public Map<String, Map> resolveMap(String requestBody) {
        log.debug("Resolving Map...");
        Map<String, Map> map;
        try {
            map = jacksonObjectMapper.readValue(requestBody, Map.class);
        } catch (Exception e) {
            //TODO may be move hardcoded message to i18 resource
            throw new RequestBodyException("incorrect request body", e);
        }
        return map;
    }
}
