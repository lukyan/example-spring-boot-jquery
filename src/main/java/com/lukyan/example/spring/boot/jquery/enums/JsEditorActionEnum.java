package com.lukyan.example.spring.boot.jquery.enums;

public enum JsEditorActionEnum {
    NEW("new"),
    EDIT("edit"),
    DELETE("delete");

    private String value;

    JsEditorActionEnum(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}
