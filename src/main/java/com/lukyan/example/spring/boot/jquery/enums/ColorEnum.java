package com.lukyan.example.spring.boot.jquery.enums;

public enum ColorEnum {
    WHITE("white"),
    BLUE("blue"),
    RED("red"),
    GREEN("green"),
    BLACK("black");

    private String value;

    ColorEnum(String value) {
        this.value = value;
    }

    public static ColorEnum byValue(String value) {
        for (ColorEnum item : ColorEnum.values()) {
            if (item.value.equals(value)) {
                return item;
            }
        }
        return null;
    }

    public String value() {
        return value;
    }

}
