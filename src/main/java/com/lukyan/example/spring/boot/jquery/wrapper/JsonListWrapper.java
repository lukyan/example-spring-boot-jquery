package com.lukyan.example.spring.boot.jquery.wrapper;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NonNull;

import java.util.*;

public class JsonListWrapper<T> {

    public static final String DEFAULT_LIST_ENTRY_NAME = "list";

    private HashMap<String, Object> properties = new HashMap<>();
    private ArrayList<T> list = new ArrayList<>();
    private String currentListEntryName = DEFAULT_LIST_ENTRY_NAME;

    public JsonListWrapper(T... items) {
        this(Arrays.asList(items));
    }

    @JsonCreator
    public JsonListWrapper(@JsonProperty("list") @NonNull Collection<T> items) {
        this.list.addAll(items);
        this.properties.put(this.currentListEntryName, this.list);
    }

    public JsonListWrapper<T> listEntryName(String listName) {
        this.properties.remove(this.currentListEntryName);
        this.currentListEntryName = listName;
        this.properties.put(this.currentListEntryName, this.list);
        return this;
    }

    public JsonListWrapper<T> withCount() {
        this.properties.put("count", this.list.size());
        return this;
    }

    @JsonIgnore
    public List<T> getList() {
        return this.list;
    }

    @JsonAnyGetter
    public Map<String, Object> getProperties() {
        return this.properties;
    }

}