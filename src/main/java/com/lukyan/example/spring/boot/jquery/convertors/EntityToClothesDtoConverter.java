package com.lukyan.example.spring.boot.jquery.convertors;

import com.lukyan.example.spring.boot.jquery.dtos.rest.ClothesDto;
import com.lukyan.example.spring.boot.jquery.jpa.entity.Clothes;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class EntityToClothesDtoConverter implements Converter<Clothes, ClothesDto> {
    @Override
    public ClothesDto convert(Clothes entity) {
        ClothesDto dto = new ClothesDto();
        Long id = entity.getId();
        if (id != null) {
            dto.setId(id);
        }
        dto.setSize(entity.getSize().value());
        dto.setPrice(entity.getPrice());
        dto.setColor(entity.getColor().value());
        dto.setType(entity.getType().value());
        dto.setDescription(entity.getDescription());
        dto.setState(entity.getState().value());
        return dto;
    }
}
