package com.lukyan.example.spring.boot.jquery.jpa.repository;

import com.lukyan.example.spring.boot.jquery.jpa.entity.Clothes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClothesRepository extends JpaRepository<Clothes, Long> {
}
