package com.lukyan.example.spring.boot.jquery.services.api;

import com.lukyan.example.spring.boot.jquery.dtos.rest.ClothesDto;
import com.lukyan.example.spring.boot.jquery.jpa.entity.Clothes;

import java.util.List;
import java.util.Map;

public interface ClothesService {
    ClothesDto resolveClothesDto(String jsonRequestBody);

    void checkParam(Object param, String paramName);

    void verifySize(Integer size);

    void verifyColor(String color);

    void verifyType(String type);

    void verifyState(String state);

    Clothes getOne(Long id);

    List<Clothes> findAll(String sortBy, String sortOrder, Integer offset, Integer limit, String keyword);

    List<Clothes> findAll();

    void delete(Long id);

    Clothes save(Map<String, Map> mapParam);

    Clothes move(ClothesDto clothesDto);

    Map<String, Map> resolveMap(String requestBody);
}
