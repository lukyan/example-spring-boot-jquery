package com.lukyan.example.spring.boot.jquery.convertors;

import com.lukyan.example.spring.boot.jquery.consts.ClothesFields;
import com.lukyan.example.spring.boot.jquery.dtos.rest.ClothesDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MapToClothesDtoConverter implements Converter<Map<String, String>, ClothesDto> {
    @Override
    public ClothesDto convert(Map<String, String> map) {
        ClothesDto dto = new ClothesDto();
        String idStr = map.get(ClothesFields.ID);
        if (idStr != null && !"".equals(idStr)) {
            dto.setId(Long.valueOf(idStr));
        }
        Integer size = Integer.valueOf(map.get(ClothesFields.SIZE));
        dto.setSize(size);
        dto.setPrice(Double.valueOf(map.get(ClothesFields.PRICE)));
        dto.setColor(map.get(ClothesFields.COLOR));
        dto.setType(map.get(ClothesFields.TYPE));
        dto.setDescription(map.get(ClothesFields.DESCRIPTION));
        dto.setState(map.get(ClothesFields.STATE));
        return dto;
    }
}
