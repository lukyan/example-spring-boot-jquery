package com.lukyan.example.spring.boot.jquery.jpa.repository.impl;

import com.lukyan.example.spring.boot.jquery.jpa.entity.Clothes;
import com.lukyan.example.spring.boot.jquery.jpa.repository.CustomClothesRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class CustomClothesRepositoryImpl implements CustomClothesRepository {

    @PersistenceContext
    private EntityManager entityManager;

    private final static String DEFAULT_SORT_BY = SortBy.createdAt;

    @Value("${default.limit}")
    private Integer defaultLimit;

    @Override
    public List<Clothes> findAll(String sortBy, String sortOrder, Integer offset, Integer limit, String keyword) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Clothes> criteriaQuery = criteriaBuilder.createQuery(Clothes.class);
        Root<Clothes> from = criteriaQuery.from(Clothes.class);
        keyword = keyword != null ? keyword.toLowerCase() : null;
        sortBy = sortBy != null ? sortBy : DEFAULT_SORT_BY;
        if (sortOrder == null) {
            switch (sortBy) {
                case SortBy.size:
                    sortOrder = SortOrder.asc;
                    break;
                case SortBy.price:
                    sortOrder = SortOrder.asc;
                    break;
                case SortBy.color:
                    sortOrder = SortOrder.asc;
                    break;
                case SortBy.type:
                    sortOrder = SortOrder.asc;
                    break;
                case SortBy.description:
                    sortOrder = SortOrder.asc;
                    break;
                case SortBy.createdAt:
                    sortOrder = SortOrder.desc;
                    break;
                case SortBy.updatedAt:
                    sortOrder = SortOrder.desc;
                    break;
            }
        }
        if (SortOrder.asc.equals(sortOrder.toLowerCase())) {
            criteriaQuery = criteriaQuery.orderBy(criteriaBuilder.asc(from.get(sortBy)));
        } else if (SortOrder.desc.equals(sortOrder.toLowerCase())) {
            criteriaQuery = criteriaQuery.orderBy(criteriaBuilder.desc(from.get(sortBy)));
        }
        if (keyword != null) {
            Predicate colorPredicate = criteriaBuilder.like(criteriaBuilder.lower(from.get("color")),
                    "%" + keyword.toLowerCase() + "%");
            Predicate typePredicate = criteriaBuilder.like(criteriaBuilder.lower(from.get("type")),
                    "%" + keyword.toLowerCase() + "%");
            Predicate descriptionPredicate = criteriaBuilder.like(criteriaBuilder.lower(from.get("description")),
                    "%" + keyword.toLowerCase() + "%");
            criteriaQuery = criteriaQuery.where(criteriaBuilder.or(colorPredicate,
                    typePredicate, descriptionPredicate));
        }
        offset = offset != null ? offset : 0;
        limit = limit != null ? limit : defaultLimit;
        TypedQuery<Clothes> query = entityManager.createQuery(criteriaQuery).setFirstResult(offset).setMaxResults(limit);
        return query.getResultList();
    }

    class SortBy {
        private final static String size = "size";
        private final static String price = "price";
        private final static String color = "color";
        private final static String type = "type";
        private final static String description = "description";
        private final static String createdAt = "createdAt";
        private final static String updatedAt = "updatedAt";
    }

    class SortOrder {
        private final static String asc = "asc";
        private final static String desc = "desc";
    }
}
