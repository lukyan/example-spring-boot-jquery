package com.lukyan.example.spring.boot.jquery.enums;

public enum SizeEnum {
    SIZE42(42),
    SIZE43(43),
    SIZE44(44),
    SIZE45(45),
    SIZE46(46),
    SIZE47(47),
    SIZE48(48),
    SIZE49(49),
    SIZE50(50),
    SIZE51(51),
    SIZE52(52),
    SIZE53(53),
    SIZE54(54);

    private int value;

    SizeEnum(int value) {
        this.value = value;
    }

    public static SizeEnum byValue(int value) {
        for (SizeEnum item : SizeEnum.values()) {
            if (item.value == value) {
                return item;
            }
        }
        return null;
    }

    public int value() {
        return value;
    }

}
