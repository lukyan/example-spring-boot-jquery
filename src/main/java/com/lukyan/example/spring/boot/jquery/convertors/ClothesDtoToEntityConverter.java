package com.lukyan.example.spring.boot.jquery.convertors;

import com.lukyan.example.spring.boot.jquery.dtos.rest.ClothesDto;
import com.lukyan.example.spring.boot.jquery.enums.ColorEnum;
import com.lukyan.example.spring.boot.jquery.enums.SizeEnum;
import com.lukyan.example.spring.boot.jquery.enums.StateEnum;
import com.lukyan.example.spring.boot.jquery.enums.TypeEnum;
import com.lukyan.example.spring.boot.jquery.jpa.entity.Clothes;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ClothesDtoToEntityConverter implements Converter<ClothesDto, Clothes> {
    @Override
    public Clothes convert(ClothesDto dto) {
        Clothes entity = new Clothes();
        Long id = dto.getId();
        if (id != null) {
            entity.setId(id);
        }
        SizeEnum sizeEnum = SizeEnum.byValue(dto.getSize());
        entity.setSize(sizeEnum);
        entity.setPrice(dto.getPrice());
        ColorEnum colorEnum = ColorEnum.byValue(dto.getColor());
        entity.setColor(colorEnum);
        TypeEnum typeEnum = TypeEnum.byValue(dto.getType());
        entity.setType(typeEnum);
        entity.setDescription(dto.getDescription());
        StateEnum stateEnum = StateEnum.byValue(dto.getState());
        entity.setState(stateEnum);
        return entity;
    }
}
