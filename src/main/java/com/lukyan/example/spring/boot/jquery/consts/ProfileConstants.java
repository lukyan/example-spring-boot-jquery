package com.lukyan.example.spring.boot.jquery.consts;

public interface ProfileConstants {
    String SPRING_PROFILE_DEVELOPMENT = "dev";
    String SPRING_PROFILE_TEST = "test";
    String SPRING_PROFILE_PRODUCTION = "prod";
}
