package com.lukyan.example.spring.boot.jquery.exceptions;

import com.lukyan.example.spring.boot.jquery.dtos.rest.ErrorResponse;
import com.lukyan.example.spring.boot.jquery.helpers.RestHelper;
import lombok.Getter;
import lombok.experimental.Delegate;

abstract public class AbstractException extends RuntimeException {

    @Getter
    @Delegate
    private ErrorResponse errorResponse = new ErrorResponse();

    public AbstractException(Throwable throwable) {
        super(throwable);
        setErrorKey(ErrorResponse.INTERNAL_SERVER_ERROR);
        setErrorMessage(throwable.getMessage());
        setDeveloperMessage(RestHelper.instance().stackTraceToString(throwable));
    }

    public AbstractException(String errorKey, String errorMessage, String developerMessage) {
        super(errorMessage);
        setErrorKey(errorKey);
        setErrorMessage(errorMessage);
        setDeveloperMessage(developerMessage);
    }

    public AbstractException(String errorKey, String errorMessage, Throwable throwable) {
        super(errorMessage);
        setErrorKey(errorKey);
        setErrorMessage(errorMessage);
        setDeveloperMessage(RestHelper.instance().stackTraceToString(throwable));
    }

    public AbstractException(String errorKey, String errorMessage) {
        super(errorMessage);
        setErrorKey(errorKey);
        setErrorMessage(errorMessage);
    }

    public AbstractException(String message, Throwable cause) {
        super(message, cause);
    }
}

