var editor;
var table;

$(document).ready(function () {
    editor = new $.fn.dataTable.Editor({
        ajax: {
            url: 'http://localhost:8080/example/api/save',
            contentType: 'application/json; charset=utf-8',
            data: function (d) {
                return JSON.stringify(d);
            }
        },
        table: "#clothesTable",
        idSrc: 'id',
        fields: [{
            label: "Id:",
            name: "id",
            enabled: false
        }, {
            label: "Size:",
            name: "size",
            type: "select",
            options: []
        }, {
            label: "Price:",
            name: "price"
        }, {
            label: "Color:",
            name: "color",
            type: "select",
            options: [{
                label: "white",
                value: "white"
            }, {
                label: "blue",
                value: "blue"
            }, {
                label: "red",
                value: "red"
            }, {
                label: "green",
                value: "green"
            }, {
                label: "black",
                value: "black"
            }
            ]
        }, {
            label: "Type:",
            name: "type",
            type: "select",
            options: []
        }, {
            label: "Description:",
            name: "description"
        }, {
            label: "State:",
            name: "state",
            type: "select",
            options: []
        }
        ]
    }).on('open', function (e, type) {
        editor.field('id').hide();

        var optionsSize = [];
        $.getJSON("http://localhost:8080/example/api/sizes", {
                term: "-1"
            },
            function (data) {
                var option = {};
                $.each(data, function (i, e) {
                    option.label = e.value;
                    option.value = e.value;
                    optionsSize.push(option);
                    option = {};
                });
            }
        ).done(function () {
            editor.field('size').update(optionsSize);
        });

        var optionsType = [];
        $.getJSON("http://localhost:8080/example/api/types", {
                term: "-1"
            },
            function (data) {
                var option = {};
                $.each(data, function (i, e) {
                    option.label = e.value;
                    option.value = e.value;
                    optionsType.push(option);
                    option = {};
                });
            }
        ).done(function () {
            editor.field('type').update(optionsType);
        });

        var optionsState = [];
        $.getJSON("http://localhost:8080/example/api/states", {
                term: "-1"
            },
            function (data) {
                var option = {};
                $.each(data, function (i, e) {
                    option.label = e.value;
                    option.value = e.value;
                    optionsState.push(option);
                    option = {};
                });
            }
        ).done(function () {
            editor.field('state').update(optionsState);
        });
    }).on('preSubmit', function (e, o, action) {
        if (action !== 'remove') {
            var priceField = this.field('price');
            /*
                        console.log(isNaN(priceField.val()));
                        var price = parseFloat(priceField.val());
                        console.log(price);
                        console.log(typeof priceField.val());
            */
            if (!priceField.isMultiValue()) {
                if (!priceField.val()) {
                    priceField.error('A price must be given');
                }
                if (isNaN(priceField.val())) {
                    priceField.error('The price must be at money format');
                }
            }
            var descriptionField = this.field('description');
            if (!descriptionField.isMultiValue()) {
                if (!descriptionField.val()) {
                    descriptionField.error('A description must be given');
                }
                if (descriptionField.val().length >= 256) {
                    descriptionField.error('The description length must be less that 256 characters');
                }
            }
            // If any error was reported, cancel the submission so it can be corrected
            if (this.inError()) {
                return false;
            }
        }
    }).on('submitComplete', function (e, type) {
        table.ajax.reload(null, false);
    });

    table = $('#clothesTable').DataTable({
        "sAjaxSource": "http://localhost:8080/example/api/",
        "sAjaxDataProp": "",
        select: {
            style: 'single'
        },
        "initComplete": function (settings, json) {
            var moveButton = table.button(3);
            moveButton.disable();
        },
        "aoColumns": [
            {"mData": "id"},
            {"mData": "size"},
            {"mData": "price"},
            {"mData": "color"},
            {"mData": "type"},
            {"mData": "description"},
            {"mData": "state"}
        ],
        "columnDefs": [
            {
                "targets": [0],
                "visible": false,
                "searchable": false
            }
        ]
    });

    new $.fn.dataTable.Buttons(table, [
        {
            extend: "create",
            editor: editor
        },
        {
            extend: "edit",
            editor: editor
        },
        {
            extend: "remove",
            editor: editor,
            action: function (e, dt, node, config) {
                var rowData = table.rows({selected: true}).data();
                var id = rowData[0].id;
                $.ajax({
                    type: "DELETE",
                    url: "http://localhost:8080/example/api/" + id,
                    success: function (data) {
                        table.ajax.reload(null, false);
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            }
        },
        {
            extend: "remove",
            editor: editor,
            text: 'Move',
            name: 'move',
            action: function (e, dt, node, config) {
                var rowData = table.rows({selected: true}).data();
                $.ajax({
                    type: "POST",
                    url: "http://localhost:8080/example/api/move",
                    data: JSON.stringify(rowData[0]),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        table.ajax.reload(null, false);
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }
    ]);

    table.buttons().container()
        .appendTo($('.col-sm-6:eq(0)', table.table().container()));
});
